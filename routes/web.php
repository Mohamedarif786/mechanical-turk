<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UrlController;
use App\Http\Controllers\Admin\GroupController;
use App\Http\Controllers\Admin\IdController;
use App\Http\Controllers\User\TestController;
use App\Http\Controllers\User\HitsController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    if(Auth::check())
    {
        return redirect('/home');
    }else{
        // return redirect('/login');
        return redirect('user/hits-list');
    }
    // return view('welcome');
});

Auth::routes();
Route::get('admin/login', [LoginController::class,'showLoginForm']);
Route::group(['middleware' => ['auth']], function () {

    Route::group(['prefix' => '/admin'], function(){

        // url-management 
        Route::get('/url',[UrlController::class,'index']);
        Route::post('/url/list/data',[UrlController::class,'listData'])->name('url.list.data');
        Route::post('/url/create',[UrlController::class,'store'])->name('url.create');
        Route::post('/url/edit',[UrlController::class,'edit'])->name('url.edit');
        Route::post('/url/delete',[UrlController::class,'delete'])->name('url.delete');

        // group-management 
        Route::get('/group',[GroupController::class,'index']);
        Route::post('/group/create',[GroupController::class,'store'])->name('group.create');
        Route::post('/group/list/data',[GroupController::class,'listData'])->name('group.list.data');
        Route::post('/group/edit',[GroupController::class,'edit'])->name('group.edit');
        Route::post('/group/delete',[GroupController::class,'delete'])->name('group.delete');

        // id-management 
        Route::get('/id-management',[IdController::class,'index']);
        Route::post('/id-management/create',[IdController::class,'store'])->name('id-management.create');
        Route::post('/id-management/list/data',[IdController::class,'listData'])->name('id-management.list.data');
        Route::post('/id-management/edit',[IdController::class,'edit'])->name('id_management.edit'); 
        Route::post('/id-management/delete',[IdController::class,'delete'])->name('id_management.delete'); 

    });

    Route::redirect('home', 'admin/url');
});


// user route 
Route::get('user/hits-list',[HitsController::class,'index']);
Route::get('user/get-single-hit/{id}',[HitsController::class,'get_single_hit']);


Route::get('test/tamper_monkey',[HitsController::class,'test_tamper_monkey']);

Route::get('/test',[TestController::class,'api']); 

