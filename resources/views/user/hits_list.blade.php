<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mechanical turk</title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('/dist/css/adminlte.min.css') }}">

    {{-- notify  --}}
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    {{-- datatable  --}}
    <link href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

    <style>
        .error {
            color: red;
        }
    </style>
</head>

<body>
    <div class="container mt-2">
        <h2 class="text-center ">Hit list</h2>

        @foreach ($get_group as $key =>$g_group)
            <h3 class="text-center">{{$g_group->name}}</h3>
            @if ($key == 0)
            <div class="table-responsive">
                <table class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image URL</th>
                            <th>Hit Time</th>
                            <th>Remaining Time</th>
                            <th>Create Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($newHits as $key => $g_hit)
                            <tr>
                                <td>5043</td>
                                <td><a href="{{ url('user/get-single-hit/' . $g_hit['HITId']) }}">Click Here</a></td>
                                <td>{{ $g_hit['AssignmentDurationInSeconds'] }}</td>
                                <td>5001</td>
                                <td>{{ $g_hit['CreationTime'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <div class="table-responsive">
                <table class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image URL</th>
                            <th>Hit Time</th>
                            <th>Remaining Time</th>
                            <th>Create Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            @endif
        @endforeach
    </div>  
</body>

</html>
