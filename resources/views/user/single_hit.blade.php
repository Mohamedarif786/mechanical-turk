
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mechanical turk</title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('/dist/css/adminlte.min.css') }}">

    {{-- notify  --}}
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    {{-- datatable  --}}
    <link href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

    <style>
        .error {
            color: red;
        }
    </style>
</head>

<body>
    <div class="container mt-2">


        @php
            // Extract HTML content from CDATA section
            $xmlObject = simplexml_load_string($get_single_hit['Question'], 'SimpleXMLElement', LIBXML_NOCDATA);
            $htmlContent = $xmlObject->HTMLContent;
        @endphp

        <div>
            {!! $htmlContent !!}
        </div>

    </div>  
</body>

</html>
