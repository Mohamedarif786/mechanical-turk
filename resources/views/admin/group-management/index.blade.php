@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Group Management</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item active">group-management</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header d-flex justify-content-end">
                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#group-form-modal">Add
                    Group</button>
            </div>
            <div class="card-body">
                <div class="table-responsive datatable text-nowrap">
                    <table class="table table-striped " id="group-list">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody class="table-border-bottom-0">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <!-- form Modal -->
    <div class="modal fade" id="group-form-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Group Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="group-form">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="id" name="id">
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="name">
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select name="status" id="status" class="form-control">
                                <option disabled selected>Select Status</option>
                                <option value="0">In active</option>
                                <option value="1">active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- delete modal  --}}

    <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 class="text-center">Are you sure you want to delete this data? </h5>
                    <div class="d-flex align-items-center justify-content-center">
                        <button type="button" class="btn btn-success btn-sm mr-2" id="delete_yes">Yes</button>
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            table = $('#group-list').dataTable({
                "oLanguage": {
                    "sEmptyTable": "No Data Available"
                },
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ route('group.list.data') }}",
                    "type": "POST",
                    data: function(d) {

                        d._token = $('meta[name="csrf-token"]').attr('content')

                    },

                },
                "columns": [{
                        "name": "name",
                        "data": "name"
                    },
                    {
                        "status": "status",
                        "render": function(data, type, row) {
                            if (row.status == "0") {
                                return `<span class="badge badge-pill badge-danger">In active</span>`;
                            } else {
                                return '<span class="badge badge-pill badge-success">active</span>';
                            }
                        }
                    },
                    {
                        "name": "action",
                        "render": function(data, type, row) {
                            return `
                              <a class="btn btn-primary btn-sm edit-group-management" href="javascript:void(0);"
                              data-bs-toggle="modal"  data-id="${row.id}"><i class="bx bx-edit-alt me-1"></i> Edit</a
                              >
                              <a class="btn btn-danger btn-sm delete-group-management" href="javascript:void(0);"
                              data-bs-toggle="modal"  data-id="${row.id}"><i class="bx bx-trash me-1"></i> Delete</a
                              >`;
                        }

                    }
                ],

                "columnDefs": [{
                        orderable: false,
                        targets: [-1]
                    },
                    {
                        searchable: false,
                        targets: [-1]
                    }
                ],
            });

            $("#group-form").validate({
                rules: {
                    name: "required",
                    status: 'required'
                },
                messages: {
                    name: "Please enter your name",
                    status: "Please Select Status",
                    id_value: "Please enter the id/value",
                },
                submitHandler: function(form) {
                    let formData = new FormData(form);
                    $.ajax({
                        url: "{{ route('group.create') }}",
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            if (data.success == 1) {
                                toaster(data.message, "success");
                                $("#group-form")[0].reset();
                                $("#group-form-modal").modal('hide');
                                table.fnDraw();
                            }
                        }
                    });
                }
            });

            $("#delete_yes").click(function() {
                var delete_id = $("#delete_yes").attr('data-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('group.delete') }}",
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        "id": delete_id
                    },
                    success: function(res) {
                        if (res.success == "1") {
                            $("#delete_modal").modal('hide');
                            toaster(res.message, "success");
                            table.fnDraw();
                        }
                    }
                });

            })
        })

        $(document).on('click', ".edit-group-management", function() {
            var id = $(this).attr('data-id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('group.edit') }}",
                type: 'POST',
                dataType: 'json',
                data: {
                    "id": id
                },
                success: function(res) {
                    if (res.success == "1") {
                        $("#id").val(res.data.id);
                        $("#name").val(res.data.name);
                        $("#status").val(res.data.status);
                        $("#group-form-modal").modal('show');
                    }
                }
            });
        })

        $(document).on('click', '.delete-group-management', function() {
            var delete_id = $(this).attr('data-id');
            $("#delete_yes").attr('data-id', delete_id);
            $("#delete_modal").modal('show');
        })
        
        $("#group-form-modal").on("hidden.bs.modal", function() {
            $("#group-form")[0].reset();
        });
    </script>
@endsection
