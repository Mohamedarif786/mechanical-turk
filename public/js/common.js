function toaster(msge, status) {
    toastr.options = {
        closeButton: true,
        progressBar: true,
    };

    if (status == "success") {
        toastr.success(msge);
    } else if (status == "warning") {
        toastr.warning(msge);
    } else {
        toastr.error(msge);
    }

}
