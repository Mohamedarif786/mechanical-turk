// ==UserScript==
// @name        NEW-CAPTCHA-2024
// @namespace   Violentmonkey Scripts
// @match       https://worker.mturk.com/*
// @match       https://amazon.com/*
// @match       https://www.amazon.com/*
// @connect     amazon.com
// @connect     harinionlinejobs.in
// @connect     2captcha.com
// @grant       GM_xmlhttpRequest
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @require     http://harinionlinejobs.in/mturk/bililiteRange.js
// @require     http://harinionlinejobs.in/mturk/jquery.sendkeys.js
// ==/UserScript==

// opfcaptcha-prod.s3.amazonaws.com

var CaptchaImageUrl='';
var CaptchaUrl='';
var CaptchaText ='';
var countCap =0;
var clickOnce =0;


if(document.getElementById('captchacharacters')) {
  document.getElementById('captchacharacters').focus();
  CaptchaImageUrl = document.getElementsByTagName('img')[0].src;
  SendImageTo2Captcha();
}
else if(document.getElementsByClassName("a-input-text a-span12 cvf-widget-input cvf-widget-input-code cvf-widget-input-captcha fwcim-captcha-guess")[0]) {
  console.log("Login Captcha Found");
  console.log(document.getElementsByClassName("a-section a-text-center cvf-captcha-img")[0].getElementsByTagName("img")[0].src);
  CaptchaImageUrl = document.getElementsByClassName("a-section a-text-center cvf-captcha-img")[0].getElementsByTagName("img")[0].src;
  SendImageTo2Captcha();
}
else {

}

 function SendImageTo2Captcha()
 {
    GM_xmlhttpRequest({
            method: "GET",
            dataType: 'json',
            url: "http://harinionlinejobs.in/2cap/img2capid.php?url="+CaptchaImageUrl,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            onload: function(response) {
                CaptchaUrl = response.response;
                CapId2Text();
            }
        });
 }

 function CapId2Text()
 {
    GM_xmlhttpRequest({
            method: "GET",
            dataType: 'json',
            url: CaptchaUrl,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            onload: function(response) {
              if(response.response.indexOf('OK') !=-1) {
                CaptchaText = response.response.replace("OK|","");
                $(document.elementFromPoint(1, 1)).click();

                if(document.getElementById('captchacharacters')) {
                  $('#captchacharacters').trigger(jQuery.Event('keypress', { keyCode: 32 })).val($("#captchacharacters")
                  .val() + String.fromCharCode(CaptchaText.charCodeAt(0),CaptchaText.charCodeAt(1),CaptchaText.charCodeAt(2),CaptchaText.charCodeAt(3),CaptchaText.charCodeAt(4),CaptchaText.charCodeAt(5)));
                  $('#captchacharacters').val("");
                  var input_op=jQuery("#captchacharacters");
                  input_op.sendkeys(CaptchaText);
                  window.scrollTo(window.innerWidth , window.innerHeight);
                  $(document.elementFromPoint(1, 1)).click();
                  setTimeout(function(){
                  triggerMouseEvent(document.getElementsByClassName("a-button-text")[0],"click");
                  }, 3000);
                }
                else if(document.getElementsByClassName("a-input-text a-span12 cvf-widget-input cvf-widget-input-code cvf-widget-input-captcha fwcim-captcha-guess")[0]) {
                  document.getElementsByClassName("a-input-text a-span12 cvf-widget-input cvf-widget-input-code cvf-widget-input-captcha fwcim-captcha-guess")[0].value = CaptchaText;
                  setTimeout(function(){
                  triggerMouseEvent(document.getElementsByClassName("a-button-input notranslate")[0],"click");
                  }, 3000);
                }
              }
              else { setTimeout(function(){  CapId2Text(); }, 4000); }
            }
        });
 }

function triggerMouseEvent(node, eventType) {
    var clickEvent = document.createEvent('MouseEvents');
    clickEvent.initEvent(eventType, true, true);
    node.dispatchEvent(clickEvent);
}












