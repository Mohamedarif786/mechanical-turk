// ==UserScript==
// @name        HOJ-Multi-2024
// @namespace   Panel
// @include     https://mturk.infoscoutinc.com/mturk/hit/*
// @require     https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @version     7
// @grant       GM_xmlhttpRequest
// @connect     ishaonlinejobs.in
// ==/UserScript==

if(document.getElementById('HIT_title'))
{
var businessname, payment ;
var total;
var purchased;
var image_url ='';
image_url = document.querySelectorAll('.receipt_images');
var totalimages='';
var imglist = [];


for(var i = 0; i < image_url.length; i++)
{
  imglist[i] = [];
  var images =$(image_url[i]).find('img.imgs');

  for(var j = 0; j < images.length; j++)
{
  imglist[i].push($(images[j]).attr('src'));

}

if(imglist[i].length > 1){
  imglist[i].push(imglist[i].join("|"));
  for(var z=1; imglist[i].length >=2;z++){
    imglist[i].splice(0, 1);
  }
}

}

totalimages = imglist.join();




var date =document.querySelectorAll('.transaction_date_validation');
var tot =document.querySelectorAll('.total_validation');
var bus =document.querySelectorAll('.banner_validation');
var pho =document.querySelectorAll('.phone_validation');
var pay =document.querySelectorAll('.payment_method_validation');


var replace ='total';
if(tot.length==0){
var tot =bus;
replace ='banner';
}

if(tot.length==0){
var tot =pho;
replace ='phone';
}

if(tot.length==0){
var tot =pay;
replace ='payment_method';
}


if(tot.length==0){
var tot =date;
replace ='transaction_datetime';
}





var overall_dates ='';

for(i=0;i< date.length;i++){

var dates ='';

 var id =$(date[i]).attr('id');



 $('select[id="'+id+'"] option').each(function(index,value) {

        dates +=$(this).val() +'|'; // dropdown option value

    });

overall_dates += dates + ',';
}

// console.log(totalimages);
// console.log(overall_dates);

var PosID = "";
var RegisterID = "";
var CashierID = "";
var StoreID = "";
var dateCheck = "";
if(document.querySelector('.transaction_date_validation')){  dateCheck = 1 ; } else{ dateCheck =0; }

if(document.querySelector('.banner_validation')){  businessname = 1 ; } else{ businessname =0; }

if(document.querySelector('.total_validation')){ total = 1 ;  } else {   total = 0; }

if(document.querySelector('.basket_qty_validation')){ item = 1 ; } else { item = 0 ; }

if(document.querySelector('.phone_validation')){ phone = 1 ; } else { phone = 0 ; }

if(document.querySelector('.payment_method_validation')){ payment = 1 ;  } else { payment = '' ; }

if(document.querySelectorAll('.pos_internal_id')) { PosID = 1 } else { PosID = ''; }

if(document.querySelectorAll('.register_id')) { RegisterID = 1  } else { RegisterID = ''; }

if(document.querySelectorAll('.cashier_id')) { CashierID = 1  } else { CashierID = ''; }

if(document.querySelectorAll('.store_no')) { StoreID = 1  } else { StoreID = ''; }


if(businessname == 0 && total == 0 && item == 0 && dateCheck == 0) {
  if(document.querySelectorAll('.pos_internal_id')) {
    var el = document.getElementsByClassName('nav_btn next_btn');
    for(var i=0; i< document.querySelectorAll('.pos_internal_id').length-1;i++)
    {
        el[i].click();
    }
    var myArray = ['80000','95000','99000','98000','97000','88000','62000','66000','77000'];
    var rand = myArray[Math.floor(Math.random() * myArray.length)];
    console.log("Hit Submit in "+ rand+ "Seconds");
    setTimeout(function(){ ScoutItSubmitCall(); }, rand);
  }
}
else {
  callgm();
}

}

function callgm(){

GM_xmlhttpRequest({
method: "POST",
dataType: 'json',
url: "https://ishaonlinejobs.in/babu1/panel/scoutit_js_server.php",
data: "url="+totalimages+"&dates="+overall_dates+"&total="+total+"&business="+businessname+"&phone="+phone+"&item="+item+"&payment="+payment+"&iframe="+encodeURIComponent(window.location.href),
headers: {
"Content-Type": "application/x-www-form-urlencoded"
},
onload: function(response) {


if(response.response.indexOf('New Record inserted successfully waiting for response...') !=-1) {
    console.log(response.response);
    setTimeout(function(){ callgm(); }, 6000);
}
else if(response.response=='Waiting') {
    console.log(response.response);
    setTimeout(function(){ callgm(); }, 6000);
}
else{
console.log(response.response);
$data=JSON.parse(response.response);

$answers =$data[0].Answers;

$individual = $answers.split(';');

var el = document.getElementsByClassName('nav_btn next_btn');

for(var i=0; i<$individual.length;i++)
{

$param = $individual[i].split('|');

$id= $(tot[i]).attr('id');

$ids =$id.replace(replace,'');

if($param[0]!='YES'){


  if( document.getElementById('banner'+$ids))
        document.getElementById('banner'+$ids).style.display='none';
if( document.getElementById('transaction_datetime'+$ids))
    document.getElementById('transaction_datetime'+$ids).style.display='none';
if( document.getElementById('total'+$ids))
     document.getElementById('total'+$ids).style.display='none';
if( document.getElementById('basket_qty'+$ids))
      document.getElementById('basket_qty'+$ids).style.display='none';
if( document.getElementById('payment_method'+$ids))
       document.getElementById('payment_method'+$ids).style.display='none';
 if( document.getElementById('phone'+$ids))
        document.getElementById('phone'+$ids).style.display='none';
    }


// else{
        if( document.getElementById('legible'+$ids))
   document.getElementById('legible'+$ids).value=$param[0];

 if( document.getElementById('payment_method'+$ids)){

       document.getElementById('payment_method'+$ids).value=$param[6];
}

 if( document.getElementById('banner'+$ids))
        document.getElementById('banner'+$ids).value=$param[3];

if( document.getElementById('transaction_datetime'+$ids))
{
  console.log($ids,$param[5]);
     document.getElementById('transaction_datetime'+$ids).value= $param[5].trim();

}



if( document.getElementById('total'+$ids))
     document.getElementById('total'+$ids).value=$param[1];

if( document.getElementById('basket_qty'+$ids))
      document.getElementById('basket_qty'+$ids).value=$param[4];


 if( document.getElementById('phone'+$ids)) {
  if($param[2]=='0') { } else {      document.getElementById('phone'+$ids).value=$param[2]; }
      }

if($param[7]!=0){
$('.last_four_box').show();
document.getElementById('last_four'+$ids).value=$param[7];
}
else
{
     if( document.getElementById('last_four'+$ids))
document.getElementById('last_four'+$ids).value='';
}
 //$('.next_btn').trigger('click');

el[i].click();

setTimeout(function(){ ScoutItSubmitCall(); }, 10000);

}


}
}

});
}



function ScoutItSubmitCall()
{
   var LastElment = document.getElementsByClassName("nav_btn submit_btn");
   var SubmitElement = LastElment[LastElment.length -1];
   SubmitElement.click();
}







