<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Id_management extends Model
{
    use HasFactory;

    public function get_group_name(){
        return $this->belongsTo('App\Models\Group_management','group_name','id');
    }
}
