<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Aws\Laravel\AwsFacade as AWS;
use Aws\Sdk;
use Aws\Mturk\MturkClient;
use Aws\Exception\AwsException;
use App\Models\Group_management;

class HitsController extends Controller
{
    //

    public function index(){
        
        // MTurk client
        $mturkClient = new MturkClient([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'sandbox' => true,
            'credentials' => [
                'key'    => 'AKIAXS3VF3T6O5VFOSLJ',
                'secret' => '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',
            ],
        ]);
        
        // Worker credentials
        $workerId = ['A1V6K2AFVMIO8Y'];
        $groupId = '3AHPWNJ8VTJN4DA5KR0N8EOIHX5YK4';

             // List HITs
             $result = $mturkClient->listHITs([
                'MaxResults' => 100,
                'HITGroupId' => $groupId,
                'Title' => $groupId,
            ]);


            // Iterate over each HIT
            foreach ($result['HITs'] as $hit) {
                // List assignments for the specific worker
                $assignments = $mturkClient->listAssignmentsForHIT([
                    'HITId' => $hit['HITId'],
                    'MaxResults' => 100,
                    'AssignmentStatuses' => ['Approved', 'Submitted','Rejected'],
                    'WorkerId' => $workerId, // Filter by Worker ID
                ]);

                if (count($assignments['Assignments']) === 0) {
                    $newHits[] = $hit;
                }
                
                // Append the assignments to the result array
                $workedHits[] = $assignments;
            }


        $get_group =Group_management::where('status',1)->get();


        return view ('user/hits_list',compact('newHits','get_group'));
    }


    public function get_single_hit($id){

        
        // MTurk client
        $mturkClient = new MturkClient([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'sandbox' => true,
            'credentials' => [
                'key'    => 'AKIAXS3VF3T6O5VFOSLJ',
                'secret' => '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',
            ],
        ]);

        $hitId = $id;


        $hit = $mturkClient->getHIT([
            'HITId'=>$hitId,
        ]);

        $get_single_hit = $hit['HIT'];

        // dd($get_single_hit);
        // $xml =simplexml_load_string() ;

        return view ('user/single_hit',compact('get_single_hit'));

    }


    public function test_tamper_monkey () {
        return view('testTamper');
    }
}
