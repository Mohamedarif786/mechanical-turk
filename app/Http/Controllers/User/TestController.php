<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Aws\Laravel\AwsFacade as AWS;
use Aws\Sdk;
use Aws\Mturk\MturkClient;
use Aws\Exception\AwsException;

class TestController extends Controller
{
    //

    public function api(){

        // MTurk client
        $mturkClient = new MturkClient([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'sandbox' => true,
            'credentials' => [
                'key'    => 'AKIAXS3VF3T6O5VFOSLJ',
                'secret' => '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',
            ],
        ]);


        // Worker credentials
        $workerId = ['A1V6K2AFVMIO8Y'];
        $groupId = '3AHPWNJ8VTJN4DA5KR0N8EOIHX5YK4';
        

        try {
            
            // List HITs
            $result = $mturkClient->listHITs([
                'MaxResults' => 100,
                'HITGroupId' => $groupId,
                'Title' => $groupId,
            ]);

            // Iterate over each HIT
            foreach ($result['HITs'] as $hit) {
                // List assignments for the specific worker
                $assignments = $mturkClient->listAssignmentsForHIT([
                    'HITId' => $hit['HITId'],
                    'MaxResults' => 100,
                    'AssignmentStatuses' => ['Approved', 'Submitted','Rejected'],
                    'WorkerId' => $workerId, // Filter by Worker ID
                ]);

                if (count($assignments['Assignments']) === 0) {
                    $newHits[] = $hit;
                }
                
                // Append the assignments to the result array
                $workedHits[] = $assignments;
            }

            dd($newHits);
            // Retrieve assignments for the specified worker
           
        
            // Filter assignments based on the worker ID
            $workerAssignments = array_filter($assignments['Assignments'], function ($assignment) use ($workerId) {
                return $assignment['WorkerId'] === $workerId;
            });
        
            // Output the list of assignments for the specified group and worker
     
        
        } catch (AwsException $e) {
            echo "Error: " . $e->getMessage() . "\n";
        }

    }
}


// $getAssignmentsResponse = $mturkClient->listAssignmentsForHIT([
//     'HITId' => '3NBFJK3IPPGNHH85KPFIXGVIKF9GO3'
// ]);
