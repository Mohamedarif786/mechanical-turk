<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Group_management;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\Datatables;

class GroupController extends Controller
{
    public function index(){
        return view('admin/group-management/index');
    }

    public function listData(Request $request){
        $url_data = Group_management::get();
        return Datatables::of($url_data)->make(true);

    }

    public function store(Request $request){
        $validators=Validator::make($request->all(),[]);

        if(!$validators->fails()){
            $message = "";
            if(isset($request->id) && $request->id){
                $group_create = Group_management::where('id',$request->id)->first();
                $message = "Group Management Update Successfully";
            }else{
                $group_create =new Group_management();
                $message = "Group Management Store Successfully";
            }
            $group_create->name =$request->name;
            $group_create->status =$request->status;
            $group_create->save();
            if($group_create){
                $response_data = ["success" => "1" ,"message" => $message];
            }else{
                $response_data = ["fail" => "0" ,"message" => "Somthing Went Wrong"];
            }
        }
        else{
            $response_data = ["success" => "0", "message" => "Site Server Error", "error" => $validators->errors()->first() ];
        }

        return response()->json($response_data);

    }

    public function edit (Request $req){
        $edit_data =Group_management::where('id',$req->id)->first();
        if($edit_data){
            $response_data = ["success" => "1","message" =>"Edit data get Successfully","data"=>$edit_data];
        }else{
            $response_data = ["failed" =>"0","message"=>"Somting went wrong"];
        }
        return response()->json($response_data);
    }

    public function delete(Request $req){
        $del_data =Group_management::where('id',$req->id)->delete();
        if($del_data){
            $response_data = ["success" => "1","message" =>"Data Deleted Successfully"];
        }else{
            $response_data = ["failed" =>"0","message"=>"Somting went wrong"];
        }
        return response()->json($response_data);
    }
}
