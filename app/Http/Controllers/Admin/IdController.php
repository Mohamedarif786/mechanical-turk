<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\Datatables;
use App\Models\Group_management;
use App\Models\Id_management;

class IdController extends Controller
{
    
    public function index(){
        $get_group =Group_management::where('status',1)->get();
        return view('admin/id-management/index',compact('get_group'));
    }

    public function listData(Request $request){
        $url_data = Id_management::with('get_group_name')->get();
        return Datatables::of($url_data)->make(true);

    }

    public function store(Request $request){
        $validators=Validator::make($request->all(),[]);

        if(!$validators->fails()){
            $message = "";
            if(isset($request->id) && $request->id){
                $id_create = Id_management::where('id',$request->id)->first();
                $message = "ID Management Update Successfully";
            }else{
                $id_create =new Id_management();
                $message = "ID Management Store Successfully";
            }
            $id_create->name =$request->name;  
            $id_create->password =$request->password;  
            $id_create->group_name =$request->group_name; 
            $id_create->status =$request->status;
            $id_create->save();
            if($id_create){
                $response_data = ["success" => "1" ,"message" => $message];
            }else{
                $response_data = ["fail" => "0" ,"message" => "Somthing Went Wrong"];
            }
        }
        else{
            $response_data = ["success" => "0", "message" => "Site Server Error", "error" => $validators->errors()->first() ];
        }

        return response()->json($response_data);
    }

    public function edit (Request $req){
        $edit_data =Id_management::where('id',$req->id)->first();
        if($edit_data){
            $response_data = ["success" => "1","message" =>"Edit data get Successfully","data"=>$edit_data];
        }else{
            $response_data = ["failed" =>"0","message"=>"Somting went wrong"];
        }
        return response()->json($response_data);
    }


    public function delete(Request $req){
        $del_data =Id_management::where('id',$req->id)->delete();
        if($del_data){
            $response_data = ["success" => "1","message" =>"Data Deleted Successfully"];
        }else{
            $response_data = ["failed" =>"0","message"=>"Somting went wrong"];
        }
        return response()->json($response_data);
    }
    
    
}
